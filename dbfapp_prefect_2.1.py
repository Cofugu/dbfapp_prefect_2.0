﻿# -*- coding: utf-8 -*-

import configparser
import os
from collections import OrderedDict
from psycopg2 import sql, extras
import shutil
import dbf
import psycopg2
import time
from pathlib import Path
from prefect import Flow, Task, task
from prefect.engine import signals
from datetime import datetime as dt

#Распоожение файла конфигурации
CONFIG_FILE = Path("D:\JOB\PyCharm_Projects\dbfapp_prefect_2.0_server\dbfapp_config.ini")

#Сохранять ли все отправки в ошибках
SAVE_ALL = True

#Настройки
LOCK_ATTEMPT_COUNTER = 10 #Ждем в секундах если есть lockfile

#Найстройки размера пакетов
BATCH_COUNT_MAX = 50000# максимальный размер пакета помещаемого во временную таблицу
TEMP_TABLE_SIZE_MAX = 5# максимальное количество пакетов в временной таблице перед отправкой

#Настройки БД
DATABASE_NAME ='pp_dbfapp'
DATABASE_USER = 'postgres'
DATABASE_HOST = 'localhost'
DATABASE_PORT = '5432'
DATABASE_PASSWORD = 'mambl'

DB_SETTINGS = f"dbname='{DATABASE_NAME}' user='{DATABASE_USER}' host='{DATABASE_HOST}' password='{DATABASE_PASSWORD}' port='{DATABASE_PORT}'"
SCHEMA_NAME = 'bekafs'

#Запросы для БД

#CODES QUERYS
CODES_FILE_NAME = "addcodes.dbf"

CODES_TABLE_NAME = "codes"

CODES_PREPARE_LIST = ("ID_C", "IN_C", "ID_T", "COD_T")

CODES_INS_BATCH = """
                INSERT INTO {0}(id_c, in_c, id_t, cod_t, shop) VALUES %s;
                """

CODES_INS_TABLE = """
                INSERT INTO {0}.{1} AS dest
                SELECT DISTINCT ON ({2}.id_c)  uuid_generate_v4(), id_c, in_c, id_t, cod_t, shop FROM {2}
                  ORDER BY id_c, SUBSTRING(in_c, 3, 15) DESC
                ON CONFLICT (id_c, shop) DO UPDATE SET
                    in_c = excluded.in_c, 
                    id_t = excluded.id_t,
                    cod_t = excluded.cod_t
                    WHERE SUBSTRING(dest.in_c, 3, 15) < SUBSTRING(excluded.in_c, 3, 15);
                """

CODES_DEL_TABLE = """
                DELETE FROM {0}.{1} AS dest
                USING {2}
                WHERE dest.id_c = {2}.id_c AND dest.shop = {2}.shop AND
                       SUBSTRING(dest.in_c, 3, 15) < SUBSTRING({2}.in_c, 3, 15);
                """

CODES_CR_TABLE = """
                CREATE TABLE IF NOT EXISTS {0}.{1}(
                    id uuid PRIMARY KEY DEFAULT uuid_generate_v4() ,
                    id_c VARCHAR(10)  NOT NULL,
                    in_c VARCHAR(28),
                    id_t VARCHAR(10),
                    cod_t VARCHAR(15),
                    shop VARCHAR(50),
                    CONSTRAINT shop_id_c UNIQUE(id_c,shop)
                    );
                """

CODES_CR_TEMP_TABLE = """
                CREATE TEMPORARY TABLE {}(
                    id_c VARCHAR(10) NOT NULL,
                    in_c VARCHAR(28),
                    id_t VARCHAR(10),
                    cod_t VARCHAR(15),
                    shop VARCHAR(50)
                    );
                """

CODES_QUERYS = (CODES_FILE_NAME,
                CODES_TABLE_NAME,
                CODES_PREPARE_LIST,
                CODES_INS_BATCH,
                CODES_INS_TABLE,
                CODES_DEL_TABLE,
                CODES_CR_TABLE,
                CODES_CR_TEMP_TABLE)



#DOCUM QUERYS
DOCUM_FILE_NAME = "adddocum.dbf"

DOCUM_TABLE_NAME = "docum"

DOCUM_PREPARE_LIST = ("MARK_D", "ID_D", "IN_D", "NAME_D", "DATA_D", "MONEYR_D", "ID_FF", "ID_FT", "NOTE")

DOCUM_INS_BATCH = """
                INSERT INTO {0}(mark_d, id_d, in_d, name_d, data_d, moneyr_d, id_ff, id_ft, note, shop) VALUES %s;
                """

DOCUM_INS_TABLE = """
                INSERT INTO {0}.{1} AS dest
                SELECT DISTINCT ON ({2}.id_d)  uuid_generate_v4(),mark_d, id_d, in_d, name_d, data_d, 
                                moneyr_d, id_ff, id_ft, note, shop FROM {2}
                ORDER BY id_d, SUBSTRING(in_d, 3, 15) DESC
                ON CONFLICT (id_d, shop) DO UPDATE SET
                    mark_d = excluded.mark_d, 
                    in_d = excluded.in_d, 
                    name_d = excluded.name_d, 
                    data_d = excluded.data_d, 
                    moneyr_d = excluded.moneyr_d, 
                    id_ff = excluded.id_ff, 
                    id_ft = excluded.id_ft, 
                    note = excluded.note
                    WHERE SUBSTRING(dest.in_d, 3, 15) < SUBSTRING(excluded.in_d, 3, 15)
                """

DOCUM_DEL_TABLE = """
                DELETE FROM {0}.{1} AS dest
                USING {2}
                WHERE dest.id_d = {2}.id_d AND dest.shop = {2}.shop AND
                       SUBSTRING(dest.in_d, 3, 15) < SUBSTRING({2}.in_d, 3, 15);
                """

DOCUM_CR_TABLE = """
                CREATE TABLE IF NOT EXISTS {0}.{1}(
                    id uuid PRIMARY KEY DEFAULT uuid_generate_v4() ,
                    mark_d VARCHAR,
                    id_d VARCHAR(10) NOT NULL,
                    in_d VARCHAR(28),
                    name_d VARCHAR(20),
                    data_d DATE,
                    moneyr_d NUMERIC(20,3),
                    id_ff VARCHAR(10),
                    id_ft VARCHAR(10),
                    note VARCHAR(60),
                    shop VARCHAR(50),
                    CONSTRAINT shop_id_d UNIQUE(id_d,shop)
                    );
                """

DOCUM_CR_TEMP_TABLE = """
                CREATE TEMPORARY TABLE {}(
                    mark_d VARCHAR,
                    id_d VARCHAR(10) NOT NULL,
                    in_d VARCHAR(28),
                    name_d VARCHAR(20),
                    data_d DATE,
                    moneyr_d NUMERIC(20,3),
                    id_ff VARCHAR(10),
                    id_ft VARCHAR(10),
                    note VARCHAR(60),
                    shop VARCHAR(50)
                    );
                """

DOCUM_QUERYS = (DOCUM_FILE_NAME,
                DOCUM_TABLE_NAME,
                DOCUM_PREPARE_LIST,
                DOCUM_INS_BATCH,
                DOCUM_INS_TABLE,
                DOCUM_DEL_TABLE,
                DOCUM_CR_TABLE,
                DOCUM_CR_TEMP_TABLE)



#DLIST QUERYS
DLIST_FILE_NAME = "adddlist.dbf"

DLIST_TABLE_NAME = "dlist"

DLIST_PREPARE_LIST = ("MARK_L", "ID_L", "IN_L", "NAME_L", "ID_D", "ID_T", "NUM_T", "PRC_T", "NOTE_L")

DLIST_INS_BATCH = """
                INSERT INTO {0}(mark_l,id_l,in_l,name_l,id_d,id_t,num_t,prc_t,note_l,shop) VALUES %s;
                """

DLIST_INS_TABLE = """
                INSERT INTO {0}.{1} AS dest
                SELECT DISTINCT ON ({2}.id_l)  uuid_generate_v4(), mark_l,id_l,in_l,name_l,id_d,id_t,num_t,prc_t,note_l, shop FROM {2}
                  ORDER BY id_l, SUBSTRING(in_l, 3, 15) DESC
                ON CONFLICT (id_l, shop) DO UPDATE SET
                    mark_l = excluded.mark_l,
                    in_l = excluded.in_l,
                    name_l = excluded.name_l,
                    id_d = excluded.id_d,
                    id_t = excluded.id_t,
                    num_t = excluded.num_t,
                    prc_t = excluded.prc_t,
                    note_l = excluded.note_l
                    WHERE SUBSTRING(dest.in_l, 3, 15) < SUBSTRING(excluded.in_l, 3, 15)
                """

DLIST_DEL_TABLE = """
                DELETE FROM {0}.{1} AS dest
                USING {2}
                WHERE dest.id_l = {2}.id_l AND dest.shop = {2}.shop AND
                       SUBSTRING(dest.in_l, 3, 15) < SUBSTRING({2}.in_l, 3, 15);
                """

DLIST_CR_TABLE = """
                CREATE TABLE IF NOT EXISTS {0}.{1}(
                    id uuid PRIMARY KEY DEFAULT uuid_generate_v4() ,
                    mark_l VARCHAR,
                    id_l VARCHAR(10) NOT NULL, 
                    in_l VARCHAR(28),
                    name_l NUMERIC(7),
                    id_d VARCHAR(10),
                    id_t VARCHAR(10),
                    num_t NUMERIC(20,3),
                    prc_t NUMERIC(20,7),
                    note_l VARCHAR(90),
                    shop VARCHAR(50),
                    CONSTRAINT shop_id_l UNIQUE(id_l,shop)
                    );
                """

DLIST_CR_TEMP_TABLE = """
                CREATE TEMPORARY TABLE {}(
                    mark_l VARCHAR,
                    id_l VARCHAR(10) NOT NULL, 
                    in_l VARCHAR(28),
                    name_l NUMERIC(7),
                    id_d VARCHAR(10),
                    id_t VARCHAR(10),
                    num_t NUMERIC(20,3),
                    prc_t NUMERIC(20,7),
                    note_l VARCHAR(90),
                    shop VARCHAR(50));
                """

DLIST_RECALC_TABLE_NAME = DOCUM_TABLE_NAME

DLIST_DOCUM_RECALC = """
                UPDATE {0}.{1} docum
                SET moneyr_d = sums.sum
                FROM (SELECT id_d,
                            SUM(dlist.num_t * dlist.prc_t) AS sum
                        FROM {0}.{2} dlist
                        WHERE trim(dlist.mark_l) = '' AND dlist.shop = {3} 
                        GROUP BY id_d) sums
                WHERE docum.id_d = sums.id_d AND docum.shop = {3};
                """

DLIST_QUERYS = (DLIST_FILE_NAME,
                DLIST_TABLE_NAME,
                DLIST_PREPARE_LIST,
                DLIST_INS_BATCH,
                DLIST_INS_TABLE,
                DLIST_DEL_TABLE,
                DLIST_CR_TABLE,
                DLIST_CR_TEMP_TABLE,
                DLIST_RECALC_TABLE_NAME,
                DLIST_DOCUM_RECALC)



#FIRMS QUERYS
FIRMS_FILE_NAME =   "addfirms.dbf"

FIRMS_TABLE_NAME = "firms"

FIRMS_PREPARE_LIST = ("ID_F", "IN_F", "NAME_F", "INN", "PR",
                "B1", "B2", "B3", "F1", "K1", "F2", "K2",
                "F3", "K3", "F4", "K4", "F5", "K5", "EMAIL",
                "NOTE_F", "KPP", "SHORTNAME", "FULLNAME", "ADDRESS",
                "BUSADDRESS", "ACCDETAILS", "ID_F_CB", "GLN", "TAGS")

FIRMS_INS_BATCH = """
                INSERT INTO {0}(id_f, in_f, name_f, inn, pr, b1, b2, b3, f1, k1, f2, k2, f3, k3, 
                f4, k4, f5, k5, email, note_f, kpp, shortname, fullname, address, busaddress, accdetails, 
                id_f_cb, gln, tags, shop) VALUES %s;
                """

FIRMS_INS_TABLE = """
                INSERT INTO {0}.{1} AS dest
                SELECT DISTINCT ON ({2}.id_f)  uuid_generate_v4(), id_f, in_f, name_f, inn, pr, b1, b2, b3, f1, k1, f2, k2, f3, k3, 
                                  f4, k4, f5, k5, email, note_f, kpp, shortname, fullname, address, busaddress, accdetails, 
                                  id_f_cb, gln, tags, shop FROM {2}
                  ORDER BY id_f, SUBSTRING(in_f, 3, 15) DESC
                ON CONFLICT (id_f, shop) DO UPDATE SET
                    in_f = excluded.in_f, 
                    name_f = excluded.name_f, 
                    inn = excluded.inn, 
                    pr = excluded.pr, 
                    b1 = excluded.b1, 
                    b2 = excluded.b2, 
                    b3 = excluded.b3, 
                    f1 = excluded.f1, k1 = excluded.k1, 
                    f2 = excluded.f2, k2 = excluded.k2, 
                    f3 = excluded.f3, k3 = excluded.k3, 
                    f4 = excluded.f4, k4 = excluded.k4, 
                    f5 = excluded.f5, k5 = excluded.k5, 
                    email = excluded.email,
                    note_f = excluded.note_f, 
                    kpp = excluded.kpp, 
                    shortname = excluded.shortname, fullname = excluded.fullname, 
                    address = excluded.address, busaddress = excluded.busaddress, accdetails = excluded.accdetails, 
                    id_f_cb = excluded.id_f_cb, gln = excluded.gln, tags = excluded.tags
                    WHERE SUBSTRING(dest.in_f, 3, 15) < SUBSTRING(excluded.in_f, 3, 15)
                """

FIRMS_DEL_TABLE = """
                DELETE FROM {0}.{1} AS dest
                USING {2}
                WHERE dest.id_f = {2}.id_f AND dest.shop = {2}.shop AND
                SUBSTRING(dest.in_f, 3, 15) < SUBSTRING({2}.in_f, 3, 15);
                """

FIRMS_CR_TABLE = """
                CREATE TABLE IF NOT EXISTS {0}.{1}(
                    id uuid PRIMARY KEY DEFAULT uuid_generate_v4() ,
                    id_f VARCHAR(10)  NOT NULL,
                    in_f VARCHAR(28),
                    name_f VARCHAR(50),
                    inn VARCHAR(15),
                    pr VARCHAR(2),
                    b1 NUMERIC(2),
                    b2 NUMERIC(2),
                    b3 NUMERIC(2),
                    f1 VARCHAR(6),
                    k1 NUMERIC(5,2),
                    f2 VARCHAR(6),
                    k2 NUMERIC(5,2),
                    f3 VARCHAR(6),
                    k3 NUMERIC(5,2),
                    f4 VARCHAR(6),
                    k4 NUMERIC(5,2),
                    f5 VARCHAR(6),
                    k5 NUMERIC(5,2),
                    email VARCHAR(35),
                    note_f VARCHAR(35),
                    kpp VARCHAR(15),
                    shortname VARCHAR(254),
                    fullname VARCHAR(254),
                    address VARCHAR(254),
                    busaddress VARCHAR(254),
                    accdetails VARCHAR(254),
                    id_f_cb VARCHAR(10),
                    gln VARCHAR(13),
                    tags VARCHAR(254),
                    shop VARCHAR(50),
                    CONSTRAINT shop_id_f UNIQUE(id_f,shop));
                """

FIRMS_CR_TEMP_TABLE = """
                CREATE TEMPORARY TABLE {}(
                    id_f VARCHAR(10) NOT NULL,
                    in_f VARCHAR(28),
                    name_f VARCHAR(50),
                    inn VARCHAR(15),
                    pr VARCHAR(2),
                    b1 NUMERIC(2),
                    b2 NUMERIC(2),
                    b3 NUMERIC(2),
                    f1 VARCHAR(6),
                    k1 NUMERIC(5,2),
                    f2 VARCHAR(6),
                    k2 NUMERIC(5,2),
                    f3 VARCHAR(6),
                    k3 NUMERIC(5,2),
                    f4 VARCHAR(6),
                    k4 NUMERIC(5,2),
                    f5 VARCHAR(6),
                    k5 NUMERIC(5,2),
                    email VARCHAR(35),
                    note_f VARCHAR(35),
                    kpp VARCHAR(15),
                    shortname VARCHAR(254),
                    fullname VARCHAR(254),
                    address VARCHAR(254),
                    busaddress VARCHAR(254),
                    accdetails VARCHAR(254),
                    id_f_cb VARCHAR(10),
                    gln VARCHAR(13),
                    tags VARCHAR(254),
                    shop VARCHAR(50)
                    );
                """

FIRMS_QUERYS = (FIRMS_FILE_NAME,
                FIRMS_TABLE_NAME,
                FIRMS_PREPARE_LIST,
                FIRMS_INS_BATCH,
                FIRMS_INS_TABLE,
                FIRMS_DEL_TABLE,
                FIRMS_CR_TABLE,
                FIRMS_CR_TEMP_TABLE)



#STORE QUERYS
STORE_FILE_NAME = 'addstore.dbf'

STORE_TABLE_NAME = "store"

STORE_PREPARE_LIST = ("_ID", "_IN", "_PID", "_KEY", "_VAL")

STORE_INS_BATCH = """
                  INSERT INTO {0}(_id, _in, _pid, _key, _val, shop) VALUES %s;
                  """

STORE_INS_TABLE = """
                INSERT INTO {0}.{1} AS dest
                SELECT DISTINCT ON ({2}._id)  uuid_generate_v4(), _id, _in, _pid, _key, _val, shop FROM {2}
                  ORDER BY _id, SUBSTRING(_in, 3, 15) DESC
                ON CONFLICT (_id, shop) DO UPDATE SET
                    _in = excluded._in, 
                    _pid = excluded._pid, 
                    _key = excluded._key, 
                    _val = excluded._val
                    WHERE SUBSTRING(dest._in, 3, 15) < SUBSTRING(excluded._in, 3, 15)
                """

STORE_DEL_TABLE = """
                DELETE FROM {0}.{1} AS dest
                USING {2}
                WHERE dest._id = {2}._id AND dest.shop = {2}.shop AND
                       SUBSTRING(dest._in, 3, 15) < SUBSTRING({2}._in, 3, 15);
                """

STORE_CR_TABLE = """
                CREATE TABLE IF NOT EXISTS {0}.{1}(
                    id uuid PRIMARY KEY DEFAULT uuid_generate_v4() ,
                    _id VARCHAR(10)  NOT NULL,
                    _in VARCHAR(28),
                    _pid VARCHAR(10),
                    _key VARCHAR(100),
                    _val VARCHAR(254),
                    shop VARCHAR(50),
                    CONSTRAINT shop_id UNIQUE(_id,shop)
                    );
                """

STORE_CR_TEMP_TABLE = """
                CREATE TEMPORARY TABLE {}(
                    _id VARCHAR(10) NOT NULL,
                    _in VARCHAR(28),
                    _pid VARCHAR(10),
                    _key VARCHAR(100),
                    _val VARCHAR(254),
                    shop VARCHAR(50)
                    );
                """

STORE_QUERYS = (STORE_FILE_NAME,
                STORE_TABLE_NAME,
                STORE_PREPARE_LIST,
                STORE_INS_BATCH,
                STORE_INS_TABLE,
                STORE_DEL_TABLE,
                STORE_CR_TABLE,
                STORE_CR_TEMP_TABLE)



#TOTYPE QUERYS
TOTYPE_FILE_NAME = "addtotyp.dbf"

TOTYPE_TABLE_NAME = "totype"

TOTYPE_PREPARE_LIST = ("MARK_Y", "ID_Y", "IN_Y", "ID_Y_N", "NAME_Y", "NAME_T", "NAME_PRN", "NAME_PRN2", "UN_T", "SHELFLIFE", "INGREDIENT", "LBL_NAMEM", "LBL_NAMEF", "VOLUME")

TOTYPE_INS_BATCH = """
                INSERT INTO {0}(mark_y, id_y, in_y, id_y_n, name_y, name_t, name_prn, name_prn2,
                  un_t, shelflife, ingredient, lbl_namem, lbl_namef, volume, shop) VALUES %s;
                """

TOTYPE_INS_TABLE = """
                INSERT INTO {0}.{1} AS dest
                SELECT DISTINCT ON ({2}.id_y)  uuid_generate_v4(), 
                  mark_y, id_y, in_y, id_y_n, name_y, name_t, name_prn, name_prn2,
                   un_t, shelflife, ingredient, lbl_namem, lbl_namef, volume, shop FROM {2}
                ORDER BY id_y, SUBSTRING(in_y, 3, 15) DESC
                ON CONFLICT (id_y, shop) DO UPDATE SET
                  mark_y = excluded.mark_y, 
                  in_y = excluded.in_y, 
                  id_y_n = excluded.id_y_n, 
                  name_y = excluded.name_y, 
                  name_t = excluded.name_t, 
                  name_prn = excluded.name_prn, 
                  name_prn2 = excluded.name_prn2,
                  un_t = excluded.un_t, 
                  shelflife = excluded.shelflife, 
                  ingredient = excluded.ingredient, 
                  lbl_namem = excluded.lbl_namem, 
                  lbl_namef = excluded.lbl_namef,
                  volume = excluded.volume
                WHERE SUBSTRING(dest.in_y, 3, 15) < SUBSTRING(excluded.in_y, 3, 15)
                """

TOTYPE_DEL_TABLE = """
                DELETE FROM {0}.{1} AS dest
                USING {2}
                WHERE dest.id_y = {2}.id_y AND dest.shop = {2}.shop AND
                       SUBSTRING(dest.in_y, 3, 15) < SUBSTRING({2}.in_y, 3, 15);
                """

TOTYPE_CR_TABLE = """
                CREATE TABLE IF NOT EXISTS {0}.{1}(
                    id uuid PRIMARY KEY DEFAULT uuid_generate_v4() ,
                    mark_y VARCHAR,
                    id_y VARCHAR(10) NOT NULL,
                    in_y VARCHAR(28),
                    id_y_n VARCHAR(10),
                    name_y VARCHAR(15),
                    name_t VARCHAR(100),
                    name_prn VARCHAR(110),
                    name_prn2  VARCHAR(110),
                    un_t VARCHAR(3), 
                    shelflife NUMERIC(3),      
                    ingredient VARCHAR(254),
                    lbl_namem VARCHAR(254),
                    lbl_namef VARCHAR(254),
                    volume NUMERIC(6,3),
                    shop VARCHAR(50),
                    CONSTRAINT shop_id_y UNIQUE(id_y,shop)
                    );
                """

TOTYPE_CR_TEMP_TABLE = """
                CREATE TEMPORARY TABLE {}(
                    mark_y VARCHAR,
                    id_y VARCHAR(10) NOT NULL,
                    in_y VARCHAR(28),
                    id_y_n VARCHAR(10),
                    name_y VARCHAR(15),
                    name_t VARCHAR(100),
                    name_prn VARCHAR(110),
                    name_prn2  VARCHAR(110),
                    un_t VARCHAR(3), 
                    shelflife NUMERIC(3),      
                    ingredient VARCHAR(254),
                    lbl_namem VARCHAR(254),
                    lbl_namef VARCHAR(254),
                    volume NUMERIC(6,3),
                    shop VARCHAR(50)
                    );
                """

TOTYPE_QUERYS = (TOTYPE_FILE_NAME,
                TOTYPE_TABLE_NAME,
                TOTYPE_PREPARE_LIST,
                TOTYPE_INS_BATCH,
                TOTYPE_INS_TABLE,
                TOTYPE_DEL_TABLE,
                TOTYPE_CR_TABLE,
                TOTYPE_CR_TEMP_TABLE)



#TOVAR QUERYS
TOVAR_FILE_NAME = "addtovar.dbf"

TOVAR_TABLE_NAME = "tovar"
#!!!record["ID_Y_S"] == 'НЕТ РАЗМЕРА'
TOVAR_PREPARE_LIST = ("ID_T", "IN_T", "ID_Y_N", "ID_Y_S", "PRC_T", "NDS", "RATE_TN", "RATE_KOL", "PERC11", "PERC12", "PERC21", "PERC22", "PCHARGE", "NOTE_T")

TOVAR_INS_BATCH = """
                INSERT INTO {0}(id_t, in_t, id_y_n, id_y_s, prc_t, 
                  nds, rate_tn, rate_kol, perc11, perc12, perc21, perc22, pcharge, note_t, shop) VALUES %s;
                """

TOVAR_INS_TABLE = """
                INSERT INTO {0}.{1} AS dest
                SELECT DISTINCT ON ({2}.id_t)  uuid_generate_v4(), id_t, in_t, id_y_n, id_y_s, prc_t, nds, rate_tn, rate_kol,
                  perc11, perc12, perc21, perc22, pcharge, note_t, shop FROM {2}
                  ORDER BY id_t, SUBSTRING(in_t, 3, 15) DESC
                ON CONFLICT (id_t, shop) DO UPDATE SET
                    in_t = excluded.in_t, 
                    id_y_n = excluded.id_y_n,
                    id_y_s = excluded.id_y_s,
                    prc_t = excluded.prc_t,
                    nds = excluded.nds,
                    rate_tn = excluded.rate_tn,
                    rate_kol = excluded.rate_kol,
                    perc11 = excluded.perc11,
                    perc12 = excluded.perc12,
                    perc21 = excluded.perc21,
                    perc22 = excluded.perc22,
                    pcharge = excluded.pcharge,
                    note_t = excluded.note_t
                    WHERE SUBSTRING(dest.in_t, 3, 15) < SUBSTRING(excluded.in_t, 3, 15)
                """

TOVAR_DEL_TABLE = """
                DELETE FROM {0}.{1} as dest
                USING {2}
                WHERE dest.id_t = {2}.id_t AND dest.shop = {2}.shop AND
                       SUBSTRING(dest.in_t, 3, 15) < SUBSTRING({2}.in_t, 3, 15);
                """

TOVAR_CR_TABLE = """
                CREATE TABLE IF NOT EXISTS {0}.{1}(
                    id uuid PRIMARY KEY DEFAULT uuid_generate_v4() ,
                    id_t VARCHAR(10) NOT NULL,
                    in_t VARCHAR(28),
                    id_y_n VARCHAR(10),
                    id_y_s VARCHAR(10),
                    prc_t NUMERIC(20,3),
                    nds NUMERIC(4,1),
                    rate_tn NUMERIC(6),
                    rate_kol NUMERIC(6),
                    perc11 NUMERIC(3),
                    perc12 NUMERIC(3),
                    perc21 NUMERIC(3),
                    perc22 NUMERIC(3),
                    pcharge NUMERIC(3),
                    note_t VARCHAR(30),
                    shop VARCHAR(50),
                    CONSTRAINT shop_id_t UNIQUE(id_t,shop)
                    );
                """

TOVAR_CR_TEMP_TABLE = """
                CREATE TEMPORARY TABLE {}(
                    id_t VARCHAR(10) NOT NULL,
                    in_t VARCHAR(28),
                    id_y_n VARCHAR(10),
                    id_y_s VARCHAR(10),
                    prc_t NUMERIC(20,3),
                    nds NUMERIC(4,1),
                    rate_tn NUMERIC(6),
                    rate_kol NUMERIC(6),
                    perc11 NUMERIC(3),
                    perc12 NUMERIC(3),
                    perc21 NUMERIC(3),
                    perc22 NUMERIC(3),
                    pcharge NUMERIC(3),
                    note_t VARCHAR(30),
                    shop VARCHAR(50)
                    );
                """

TOVAR_QUERYS = (TOVAR_FILE_NAME,
                TOVAR_TABLE_NAME,
                TOVAR_PREPARE_LIST,
                TOVAR_INS_BATCH,
                TOVAR_INS_TABLE,
                TOVAR_DEL_TABLE,
                TOVAR_CR_TABLE,
                TOVAR_CR_TEMP_TABLE)



#TOTDOCUM QUERYS
TOTDOCUM_FILE_NAME = "totdocumex.dbf"

TOTDOCUM_TABLE_NAME = "totdocum"

TOTDOCUM_PREPARE_LIST = ("ID_F", "MONEYR_D")

TOTDOCUM_INS_BATCH = """
                INSERT INTO {0}(id_f, moneyr_d, shop) VALUES %s;
                """

TOTDOCUM_INS_TABLE = """
                INSERT INTO {0}.{1} AS dest
                    SELECT DISTINCT ON ({2}.id_f, {2}.shop)  id_f, moneyr_d, shop FROM {2}
                      ORDER BY id_f DESC
                    ON CONFLICT (id_f, shop) DO UPDATE SET
                        moneyr_d = excluded.moneyr_d,
                        shop = excluded.shop;
                     """

TOTDOCUM_DEL_TABLE = """
                DELETE FROM {0}.{1} as dest
                USING {2}
                WHERE dest.id_f = {2}.id_f AND dest.shop = {2}.shop;
                """

TOTDOCUM_CR_TABLE = """
                CREATE TABLE IF NOT EXISTS {0}.{1}(
                    id_f VARCHAR(10) NOT NULL,
                    moneyr_d  NUMERIC(20,3),
                    shop VARCHAR(50),
                    PRIMARY KEY (id_f, shop)
                    );
                """

TOTDOCUM_CR_TEMP_TABLE = """
                CREATE TEMPORARY TABLE {}(
                    id_f VARCHAR(10) NOT NULL,
                    moneyr_d  NUMERIC(20,3),
                    shop VARCHAR(50)
                    );
                """

TOTDOCUM_QUERYS = (TOTDOCUM_FILE_NAME,
                TOTDOCUM_TABLE_NAME,
                TOTDOCUM_PREPARE_LIST,
                TOTDOCUM_INS_BATCH,
                TOTDOCUM_INS_TABLE,
                TOTDOCUM_DEL_TABLE,
                TOTDOCUM_CR_TABLE,
                TOTDOCUM_CR_TEMP_TABLE)

#Описание классов конфигурации
class ShopConfiguration:
    def __init__(self, shop_section: OrderedDict):
        self.shop_name = shop_section['shop_name']
        self.adding_order = shop_section['adding_order'].split(', ')
        self.data_path = shop_section['data_path'] / Path(self.shop_name)
        self.saved_path = self.data_path / Path('saved')
        self.in_path = self.data_path / Path('in')

class Configuration():
    def __init__(self, file_name):
        if not file_name.is_file():
            raise FileNotFoundError(f"Configuration file isn't found in {file_name}")
        self.shops = list()
        config = configparser.ConfigParser()
        config.read(file_name)
        for sect in config.sections():
            self.shops.append(ShopConfiguration(config[sect]))

class Result:
    def __init__(self):
        self.total = 0
        self.success = 0
        self.ignored = 0

class Loader(Task):
    def __init__(self, shop: ShopConfiguration, querys, *args, **kwargs):
        self.shop: ShopConfiguration = shop
        self.querys = querys
        self.shop_name = shop.shop_name
        self.file_name = self.querys[0]
        self.table_name = self.querys[1]
        super().__init__(*args, **kwargs)

    def run(self, is_here, reserved, deleted: bool = False):
        self.logger.info(f"Загружаем {self.shop_name}.{self.file_name}")
        self.deleted = deleted
        self.result = Result()
        self.connect()
        data = dbf.Table(str(self.shop.in_path / Path(self.file_name)))
        data.open()
        # создаем таблицу в БД, если ее нет
        self.create_table()
        # названия временных таблиц используемых для вставки (add_tovar или del_tovar)
        add_table_name = 'del_' + self.table_name if self.deleted else 'add_' + self.table_name
        self.create_temporary_table(add_table_name)

        batch_count: int = 0
        table_size: int = 0#Количество батчей в временной таблице
        batch: list = []

        for record in data:
            # xor
            if deleted ^ dbf.is_deleted(record):
                continue
            line = self.prepare_record(record)
            batch.append(tuple(list(map(lambda s: s.encode().decode('utf-8') if type(s) is str else s, line))))
            batch_count += 1
            #вставляем батч если он переполняется
            if batch_count >= BATCH_COUNT_MAX:
                self.insert_batch(add_table_name, batch)
                self.logger.info(f'Вставлено во временную таблицу {add_table_name}  - {batch_count} строк')
                batch = []
                batch_count = 0
                table_size += 1
            # вставляем временную таблицу в БД если она переполняется
            if table_size >= TEMP_TABLE_SIZE_MAX:
                self.insert_table(add_table_name)
                self.logger.info(f'Загружена временная таблица {add_table_name} из {table_size * BATCH_COUNT_MAX} записей')
                table_size = 0
            self.result.total += 1
        if batch_count > 0:
            self.insert_batch(add_table_name, batch)
            self.logger.info(f'Вставлено во временную таблицу {add_table_name}  - {batch_count} строк')
            self.insert_table(add_table_name)
            self.logger.info(f'Загружена временная таблица {add_table_name} из {batch_count} записей')
            if self.cursor.rowcount != -1:
                self.result.success += self.cursor.rowcount
            self.result.ignored = self.result.total - self.result.success
        data.close()
        self.commit()
        self.close_connection()

        if self.file_name == 'adddlist.dbf':
            self.connect()
            self.recalc()
            self.commit()
            self.close_connection()

        self.print_result()

        return True

    def connect(self):
        self.connection = psycopg2.connect(DB_SETTINGS)
        self.cursor = self.connection.cursor()

    def commit(self):
        self.connection.commit()

    def close_connection(self):
        if self.cursor is not None:
            self.cursor.close()
        if self.connection is not None:
            self.connection.close()

    def prepare_record(self, record: dbf.Record):
        line = []
        for i in range(len(self.querys[2])):
            line.append(record[self.querys[2][i]])
        line.append(self.shop_name)
        return self.clear_nul_chars(line)

    @staticmethod
    def clear_nul_chars(line: list):
        return [(s.replace('\x00', '') if type(s) is str else s) for s in line]

    def create_table(self):
        self.cursor.execute(
            sql.SQL(self.querys[6]).format(sql.Identifier(SCHEMA_NAME), sql.Identifier(self.table_name)))

    def create_temporary_table(self , add_table_name):
        self.cursor.execute(
            sql.SQL(self.querys[7]).format(sql.Identifier(add_table_name)))

    def insert_batch(self, add_table_name, batch):
        extras.execute_values(self.cursor,
            sql.SQL(self.querys[3]).format(sql.Identifier(add_table_name)).as_string(self.cursor), batch)

    def insert_table(self, add_table_name):
        if not self.deleted:
            self.cursor.execute(sql.SQL(self.querys[4]).format(sql.Identifier(SCHEMA_NAME), sql.Identifier(self.table_name), sql.Identifier(add_table_name)))
        else:
            # delete from database
            self.cursor.execute(sql.SQL(self.querys[5]).format(sql.Identifier(SCHEMA_NAME), sql.Identifier(self.table_name), sql.Identifier(add_table_name)))
        if self.cursor.rowcount != -1:
            self.result.success += self.cursor.rowcount
        self.cursor.execute(f"TRUNCATE TABLE {add_table_name}")

    def print_result(self):
        if not self.deleted:
            self.logger.info(f"Результаты вставки в {self.shop_name}.{self.file_name}")
            self.logger.info(f' + на вставку         - {str(self.result.total)}')
            self.logger.info(f' + было вставлено     - {str(self.result.success)}')
            self.logger.info(f' + проигнорировано    - {str(self.result.ignored)}')
        else:
            self.logger.info(f"Результаты удаления в {self.shop_name}.{self.file_name}")
            self.logger.info(f' + на удаление        - {str(self.result.total)}')
            self.logger.info(f' + было удалено       - {str(self.result.success)}')
            self.logger.info(f' + проигнорировано    - {str(self.result.ignored)}')

    #только для пересчета docum при загрузке Dlist
    def recalc(self):
        self.cursor.execute(
            sql.SQL(DOCUM_QUERYS[6]).format(sql.Identifier(SCHEMA_NAME), sql.Identifier(DOCUM_QUERYS[1])))
        self.cursor.execute(
            sql.SQL(self.querys[9]).format(sql.Identifier(SCHEMA_NAME), sql.Identifier(self.querys[8]),
                    sql.Identifier(self.table_name), sql.Literal(self.shop_name)))
        self.logger.info(f"Пересчет {self.querys[8]}")


class Reserve_Dir(Task):
    def __init__(self, shop, *args, **kwargs):
        self.shop = shop
        super().__init__(*args, **kwargs)
    def run(self):
        if not Path(self.shop.data_path).is_dir():
            raise signals.SKIP(f"Directory '{self.shop.shop_name}' doesn't exist")
        # вместо использования блокировки, будем смотреть на файлы
        # названия файлов из сервиса beka-file-server
        # создадим файл lockedBy1C.dbf
        signalfile = Path(self.shop.in_path / Path('lockedBy1C.dbf'))
        signalfile.touch()
        lockfile = Path(self.shop.in_path / Path('lockedByBeka.dbf'))
        # попробуем подождать если существует lockfile
        lock_count = 0
        while lockfile.is_file() and lock_count <= LOCK_ATTEMPT_COUNTER:
            lock_count += 1
            time.sleep(1)
        # подождали
        if lockfile.is_file():
            signalfile.unlink()
            raise signals.SKIP(f"Path {shop.in_path} is locked by file-server (lockedByBeka.dbf)")
        self.logger.info(f'Directory "{self.shop.shop_name}" reserved')
        return True

class Unreserve_Dir(Task):
    def __init__(self, shop, *args, **kwargs):
        self.shop = shop
        super().__init__(*args, **kwargs)
    def run(self, finished):
        signalfile = Path(self.shop.in_path / Path('lockedBy1C.dbf'))
        if signalfile.is_file():
            signalfile.unlink()


class Is_File(Task):
    def __init__(self, shop, file_name, *args, **kwargs):
        self.shop = shop
        self.file_name = file_name
        super().__init__(*args, **kwargs)
    def run(self):
        if Path(self.shop.in_path / Path(self.file_name)).is_file():
            return True
        else:
            raise signals.SKIP()

class Save_File(Task):
    def __init__(self, shop, file_name, *args, **kwargs):
        self.shop = shop
        self.file_name = file_name
        super().__init__(*args, **kwargs)
    def run(self, is_here, dir_name):
        if is_here:
            shutil.copy(self.shop.in_path / Path(self.file_name), self.shop.saved_path / Path(dir_name) / Path(self.file_name))
        return True

class Remove_File(Task):
    def __init__(self, shop, file_name, *args, **kwargs):
        self.shop = shop
        self.file_name = file_name
        super().__init__(*args, **kwargs)
    def run(self, insert_finished, delete_finished, saved):
        if insert_finished and delete_finished and os.path.exists(self.shop.in_path / Path(self.file_name)):
            os.remove(self.shop.in_path / Path(self.file_name))
        return True

class Mk_Save_Dir(Task):
    def __init__(self, shop, *args, **kwargs):
        self.shop = shop
        super().__init__(*args, **kwargs)
    def run(self, reserved):
        if os.listdir(self.shop.in_path) == ['lockedBy1C.dbf']:
            raise signals.SKIP()
        if reserved:
            if not Path(self.shop.saved_path).is_dir():
                os.mkdir(self.shop.saved_path)
            dir_name = dt.now().strftime("%b_%d_%Y_%H.%M.%S")
            os.mkdir(self.shop.saved_path / Path(dir_name))
            return dir_name

with Flow("dbf-Pg_loader") as main:
    conf = Configuration(CONFIG_FILE)
    for shop in conf.shops:
        reserve_dir = Reserve_Dir(shop, name="res_" + shop.shop_name)
        reserved = reserve_dir()
        if SAVE_ALL:
            mk_save_dir = Mk_Save_Dir(shop, name="mk_sv_dir_" + shop.shop_name)
            dir_name = mk_save_dir(reserved)
        finished = []
        for file_name in shop.adding_order:
            is_file = Is_File(shop, file_name, name="is_" + file_name)
            is_here = is_file()
            querys = {
                'addfirms.dbf': FIRMS_QUERYS,
                'addstore.dbf': STORE_QUERYS,
                'adddocum.dbf': DOCUM_QUERYS,
                'adddlist.dbf': DLIST_QUERYS,
                'addcodes.dbf': CODES_QUERYS,
                'addtovar.dbf': TOVAR_QUERYS,
                'addtotyp.dbf': TOTYPE_QUERYS,
                'totdocumex.dbf': TOTDOCUM_QUERYS,
            }

            loader = Loader(shop, querys[file_name], name=querys[file_name][1].upper() + "_Loader")

            insert_finished = loader(is_here, reserved)
            delete_finished = loader(is_here, reserved, deleted = True)
            if SAVE_ALL:
                save_file = Save_File(shop, file_name, name="sv_" + file_name)
                saved = save_file(is_here, dir_name)
            else:
                saved = True
            remove_file = Remove_File(shop, file_name, name="rm_" + file_name)
            finished.append(remove_file(insert_finished, delete_finished, saved))
        unreserve_dir = Unreserve_Dir(shop, skip_on_upstream_skip=False, name="unres_" + shop.shop_name)
        unreserve_dir(finished)

if __name__ == "__main__":
    main.register(project_name="dbfapp_prefect_2.0")
    #main.run()
