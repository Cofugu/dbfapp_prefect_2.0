1)В файле dbfapp_config.ini задать путь к папке data_exchange
param: data_path = .../data_exchange
В папке data_exchange должны быть папки магазинов с папками in и saved внутри
data_exchange/bermag/in
		    /saved
	     /bashmag/in
		     /saved
	     /...    /...
2)В файле dbfapp_prefect.py указать местоположение файла dbfapp_config.ini и настройки базы Postgres, при необходимости задать другие настройки
3)Убедиться, что установлены все пакеты python из requirements.txt
4)Запустить prefect server и prefect agent, создать в prefect API (по адресу http://<адрес сервера>:<порт prefect'а, стандартно 8080>/) проект с названием abfapp_prefect
5)Запустить dbfapp_prefect.py
Открываем проект и поток Main, запускаем его
Через prefect API можем настроить переодический запуск через уcтановку shedule